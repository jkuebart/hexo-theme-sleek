Sleek
=====

A minimalistic single-column monochrome blogging theme for [Hexo][HEXO]. It
is designed for desktop and mobile without being responsive.


Installation
------------

Install and use sleek as follows from the main folder of your hexo site:

    git clone https://gitlab.com/jkuebart/hexo-theme-sleek themes/sleek
    npx hexo config theme sleek

Sleek is written in nunjucks and can be rendered using Hexo's built-in swig
renderer. Consequently, you can remove some components from the default
installation if your site isn't using them:

    npm uninstall hexo-renderer-ejs hexo-renderer-stylus

Optionally, if you would like to take advantage of [lunr][LUNR] full-text
search, you should enable `lunr` in the theme configuration (see below) and
additionally install lunr itself:

    npm install lunr


Configuration
-------------

Sleek can include the required CSS file for Hexo's default code highlighter
[highlight.js][HLJS]. The theme can be configured as explained below. The
unmodified style files require the `hljs` prefix for class names, so you
must add the following to your Hexo configuration:

    highlight:
        hljs: true

Sleek can be configured through your site's `_config.yml`. All
configuration is optional.

    theme: "sleek"
    theme_config:
        archives:
            format: "MMMM Y"
            show_count: true
            type: "monthly"

        categories:
            depth: 0
            show_count: true

        copyright:
            year: 2020
            email: "a@abc.com"

        highlight:
            style: "default"

        lunr: false

        motto:
            quote: "The road goes ever on an on."
            author: "J.R.R. Tolkien"
            title: "The Fellowship of the Ring."

        tags:
            show_count: true

  * `archives`: Appearance of the archives page (see below).
      - `format`: The date format for "monthly" archives, default "MMMM Y".
      - `show_count`: Show the number of posts in each archive section
        (default `true`).
      - `type`: The granularity of the archive, can be "yearly" or
        "monthly", the default. In order to use "yearly", `format` must be
        unset or empty.

  * `categories`: Appearance of the categories listing (see below).
      - `depth`: Levels of categories to be displayed. 0 displays all
        categories and child categories; -1 is similar to 0 but displayed
        in flat; 1 displays only top level categories. Defaults to 0.
      - `show_count`: Display the number of posts in each category, default
        `true`.

  * `copyright`: Copyright shown on each page.
      - `year`: Optional copyright year (or years).
      - `email`: Optional e-mail address, linked to `author`.

  * `highlight`:
      - `style`: The style to use for the default highlighter, see
        [highlight.js][HLJS]. Defaults to "default".

  * `lunr`: Whether to provide full-text search using [lunr][LUNR].
    Requires the `lunr` package to be installed. Defaults to `false`.

  * `motto`: Your site can have an optional motto that is shown on the
    index page.
      - `quote`: The quote for the motto.
      - `author`: The author of the quote (optional).
      - `title`: The source of the quote (optional).

  * `tags`: Appearance of the tags listing and at the bottom of every post.
      - `show_count`: Display the number of posts for each tag, default
        `true`.


Archives, categories and tags
-----------------------------

Sleek's main index page doesn't provide access to archives or the category
and tag listings. If you like, you can create [pages][PAGE] for them by
using an appropriate layout.

For example, create a file `source/archives.md` with the following
contents:

    ---
    layout: "archives"
    title: "Archives"
    ---

Use the layouts "categories", "tags" or "tagcloud" for the other listings,
respectively.


Work in progress
----------------

  * Only the "tagcloud" layout is usable. The layouts for "archives",
    "categories" and "tags" are unfinished and look very ugly.

  * »Gallery posts« with a list of `photos` in the front-matter aren't
    supported yet.

  * No comment support.

  * Atom feeds aren't supported.


Licence
-------

Sleek is released under the [3-clause BSD licence][BSD3], see
[LICENCE][LICENCE].


[BSD3]: https://opensource.org/licenses/BSD-3-Clause
[HEXO]: https://hexo.io/
[HLJS]: https://highlightjs.org/static/demo/
[LICENCE]: https://gitlab.com/jkuebart/hexo-theme-sleek/raw/master/LICENCE
[LUNR]: https://lunrjs.com/
[PAGE]: https://hexo.io/docs/writing#Layout
