/*
 * Copyright 2020, Joachim Kuebart <joachim.kuebart@gmail.com>
 * See https://gitlab.com/jkuebart/hexo-theme-sleek/raw/master/LICENCE
 */

/*global
    console, lunr
*/


(function () {
    "use strict";

    const {
        inputSelector,
        jsonUrl,
        resultsSelector
    } = document.currentScript.dataset;

/// Create a DOM element.
/// @param elem The name of the element, or an object with attributes.
/// @param children The children to be inserted.

    function elem(elem, ...children) {
        if ("string" === typeof elem) {
            elem = {name: elem};
        }
        const node = document.createElement(elem.name);
        Object.entries(elem).forEach(function ([name, value]) {
            if ("name" !== name) {
                node.setAttribute(name, value);
            }
        });
        node.append(...children);
        return node;
    }

    let searchIndex;

/// Search for the given term.
/// @param term The search term.
/// @return A Promise for the list of results as DOM elements.

    function lunrSearch(term) {

// Load search index on first search.

        if (!searchIndex) {
            searchIndex = fetch(
                jsonUrl
            ).then(
                (response) => response.json()
            ).then(
                function ({index, posts}) {
                    return {
                        index: lunr.Index.load(index),
                        posts
                    };
                }
            );
        }

// Perform search on input.

        return searchIndex.then(function ({index, posts}) {
            return index.search(term).map(
                (match) => posts[match.ref]
            ).map(
                (post) => elem(
                    "li",
                    elem("time", post.date),
                    elem({name: "a", href: post.path}, post.title)
                )
            );
        });

    }

    function attach() {
        const input = document.querySelector(inputSelector);
        const results = document.querySelector(resultsSelector);
        const originalIndex = Array.prototype.slice.call(results.children);
        function onInput() {

// Clear error message.

            input.setCustomValidity("");
            const term = input.value.trim();

// Show original index when search box is empty.

            const matches = (
                "" === term
                ? originalIndex
                : lunrSearch(term)
            );

// Show results.

            Promise.resolve(matches).catch(
                function (error) {
                    console.log(error);
                    input.setCustomValidity(error.message);
                    return [];
                }
            ).then(
                function (matches) {
                    while (results.firstChild) {
                        results.firstChild.remove();
                    }
                    results.append(...matches);
                }
            );

        }

// Update result list if search box is non-empty.

        if ("" === input.value.trim()) {
            const query = (new URL(document.location)).searchParams.get("q");
            if ("" !== query) {
                input.value = query;
            }
        }
        if ("" !== input.value.trim()) {
            onInput();
        }
        input.addEventListener("input", onInput);
    }

// Delay running until lunr has loaded.

    if ("complete" === document.readyState) {
        attach();
    } else {
        document.addEventListener("readystatechange", attach);
    }
}());
