"use strict";

// If there is no nunjucks renderer, use swig instead. Old versions of hexo
// came with swig built-in.
const swig = hexo.extend.renderer.get("swig");
if (!hexo.extend.renderer.get("njk") && swig) {
    hexo.log.debug("Substituting swig for njk.");
    const swigSync = hexo.extend.renderer.get("swig", true);
    if (swigSync) {
        hexo.extend.renderer.register("njk", swigSync.output, swigSync, true);
    } else {
        hexo.extend.renderer.register("njk", swig.output, swig);
    }
}
