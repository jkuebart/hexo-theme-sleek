"use strict";

// If the default highlighter is enabled, include its style.
// Original styles require the "hljs" prefix.
const {enable, hljs} = hexo.config.highlight || {};
if (enable) {
    if (!hljs) {
        hexo.log.error("Style file requires highlight.hljs = true.");
    }

    const fs = require("hexo-fs");
    hexo.extend.generator.register(
        "highlighter-style",
        function () {
            const {style} = hexo.theme.config.highlight || {};
            return {
                path: "css/highlight.css",
                data: () => fs.createReadStream(require.resolve(
                    `highlight.js/styles/${style || "default"}.css`
                ))
            };
        }
    );
}
