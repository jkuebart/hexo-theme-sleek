/*property
    _content, add, async, boost, call, class, config, content, create,
    createReadStream, data, date, extend, field, filter, forEach, generator,
    get, helper, htmlTag, id, index, join, lunr, map, name, page, pages, path,
    placeholder, posts, register, resolve, resultsSelector, src, stringify,
    tags, text, theme, title, toArray, toJSON, value
*/

"use strict";

const {htmlTag} = require("hexo-util");
const fs = require("hexo-fs");

const dateHelper = hexo.extend.helper.get("date");
const js = hexo.extend.helper.get("js");
const url_for = hexo.extend.helper.get("url_for");

// Format a post for display in search results.

function formatPost(post) {
    const ctx = Object.create(hexo, {page: {value: post}});
    return {
        date: dateHelper.call(ctx, post.date),
        path: url_for.call(ctx, post.path),
        title: post.title
    };
}

// This Hexo generator produces the search index and the lunr JavaScript
// files.

function lunrGenerator(locals) {
    if (!hexo.theme.config.lunr) {
        return [];
    }

// Collect all indexable posts and pages. The index in this array is the ID.

    const postsAndPages = [
        ...locals.posts.toArray(),
        ...locals.pages.toArray()
    ];
    const postsToIndex = postsAndPages.filter(
        (post) => post._content || post.content
    );

    const lunr = require("lunr");
    const index = lunr(function (builder) {
        builder.field("content");
        builder.field("tags", {boost: 2});
        builder.field("title", {boost: 4});

// When available, index the markdown source code (_content) instead of the
// formatted HTML (content).

        postsToIndex.forEach(
            (post, id) => builder.add({
                id,
                content: post._content || post.content,
                tags: post.tags,
                title: post.title
            })
        );
    });

// Generate the JavaScript file for the client and the index.

    return [
        {
            path: "js/lunr.js",
            data: () => fs.createReadStream(require.resolve("lunr"))
        },
        {
            path: "lunr.json",
            data: JSON.stringify({
                index: index.toJSON(),
                posts: postsToIndex.map(formatPost)
            })
        }
    ];
}

// This Hexo helper creates the search box and loads the required
// JavaScript files.

function lunrHelper(options) {
    if (!hexo.theme.config.lunr) {
        return [];
    }

    options = options || {};
    const className = options.class || "search-form";
    const {resultsSelector, text} = options;

    return [
        htmlTag(
            "form",
            {},
            htmlTag(
                "input",
                {
                    class: className,
                    name: "q",
                    placeholder: text || "Search…"
                }
            ),
            false
        ),
        js.call(
            hexo,
            {async: true, src: "js/lunr.js"},
            {
                async: true,
                "data-input-selector": `input.${className}`,
                "data-json-url": url_for.call(hexo, "lunr.json"),
                "data-results-selector": resultsSelector,
                src: "js/search.js"
            }
        )
    ].join("\n");
}

hexo.extend.generator.register("lunr", lunrGenerator);
hexo.extend.helper.register("lunr_search", lunrHelper);
